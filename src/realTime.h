/** @file realTime.h
    @brief Header to the realTime.
*/

#ifndef _REALTIME_H_
#define _REALTIME_H_

#include <ESP8266WiFi.h>
#include <NTPClient.h>
#include <WiFiUdp.h>

class RealTime {
    public:
        RealTime();
        void        begin();
        bool        update();
        uint32_t    get_timestamp();

    private:
        WiFiUDP     ntpUDP;
        NTPClient   timeClient;
        uint32_t    timestamp;
        uint32_t    millisTime;
};

#endif  /* _REALTIME_H_ */
