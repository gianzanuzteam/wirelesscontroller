#include <ESP8266WiFi.h>
#include <SPI.h>
#include "serverApi.h"
#include "realTime.h"
#include "messages.h"
#include "CRC.h"

/* Constantes */
#define WIFI_INIT_DELAY         (15000)
#define DEBUG_OUTPUT

/* Variáveis privadas */
const uint8_t       sourceId[]      = {0xef, 0xff, 0x3d, 0x4a, 0x97};
const uint8_t       destinationId[] = {0xef, 0xff, 0x3d, 0x4a, 0x97};

/* Objetos */
Messages            messages(sourceId, destinationId);
ServerApi           serverApi;
RealTime            realTime;

void setup()
{
    /* GPIOS */
    pinMode(LED_EN, INPUT_PULLUP);

    /* Interrupt */
    pinMode(IRQ_NRF, INPUT);

    /* Inicializa periféricos */
    Serial.begin(115200);
    SPI.begin();

    /* Inicializa WiFi */
    WiFi.mode(WIFI_STA);
    delay(1000);
    WiFi.begin();

    /* Aguarda conectar ao WiFi */
    while (!WiFi.isConnected())
    {
        /* Verifica se o botão foi pressionado */
        if (!digitalRead(LED_EN))
        {
            /* Aguarda liberar o botão */
            while (!digitalRead(LED_EN)) {
                delay(100);
            };

            /* Inicializa WPS */
            WiFi.beginWPSConfig();

#ifdef DEBUG_OUTPUT
            Serial.println("DEBUG: WPS ON");
#endif

            /* Habilita LED */
            pinMode(LED_EN, OUTPUT);

            /* Aguarda conectar ao WiFi */
            while (!WiFi.isConnected())
            {
                digitalWrite(LED_EN, LOW);
                delay(500);
                digitalWrite(LED_EN, HIGH);
                delay(500);
            }

            /* Finaliza interação */
            break;
        }
        delay(500);
    }

#ifdef DEBUG_OUTPUT
    Serial.println("DEBUG: WIFI CONNECTED");
#endif

    /* LED */
    digitalWrite(LED_EN, HIGH);
    pinMode(LED_EN, OUTPUT);

    /* Inicializa RTC */
    realTime.begin();
    realTime.update();

    /* Inicializa mensagens */
    messages.begin(Messages::MESSAGE_MODE_CONTROLLER);

    /* inicializa servidor */
    serverApi.begin();
}

void loop()
{
    Messages::payload_t payload;
    if(messages.receive(&payload))
    {
#ifdef DEBUG_OUTPUT
        Serial.println("DEBUG: MESSAGE RECEIVED");
#endif
        /* Processa mensagem */
        digitalWrite(LED_EN, LOW);
        serverApi.proccess_RX(&payload, realTime.get_timestamp());
        digitalWrite(LED_EN, HIGH);

#ifdef DEBUG_OUTPUT
        Serial.println("DEBUG: MESSAGE PROCESSED");
#endif
    }

    /* Atualiza timestamp */
    realTime.update();

    /* Processa pacotes para servidor */
    serverApi.proccess_TX();
}
