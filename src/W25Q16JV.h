/** @file W25Q16JV.h
    @brief Header to the W25Q16JV NOR Serial Flash SPI by Winbond.
*/

#ifndef _W25Q16JV_H_
#define _W25Q16JV_H_

/*************************************************************************************
    Includes
*************************************************************************************/
#include <stdint.h>

/** @defgroup W25Q16JV_FLASH W25Q16JV FLASH memory related.
    Functions for the W25Q16JV FLASH memory.
    @{ */

/*************************************************************************************
    Public macros
*************************************************************************************/
#define W25Q16JV_SECTOR_BYTE_SIZE               (4096)
#define W25Q16JV_PAGE_BYTE_SIZE                 (256)
#define W25Q16JV_MAX_PAGE_COUNT                 (8192)
#define W25Q16JV_MAX_ADDRESS_TOTAL              (W25Q16JV_MAX_PAGE_COUNT*W25Q16JV_PAGE_BYTE_SIZE)

/*************************************************************************************
    Public variables
*************************************************************************************/

/*************************************************************************************
    Classes
*************************************************************************************/
class W25Q16JV {
    public:

        W25Q16JV() = default;
        bool init(void);
        bool read_data(uint8_t* data, uint32_t size, uint32_t address);
        bool write_data(uint8_t* data, uint32_t size, uint32_t address);
        bool erase_sector(uint32_t address);
        bool erase_chip(uint32_t address);
        void wakeup(void);
        void sleep(void);

    private:

        bool isAwake = false;
        const uint8_t JEDEC_ID[3] = {0xEF, 0x40, 0x15};
};

/*************************************************************************************
    Public prototypes
*************************************************************************************/

/** @} */
#endif  /* _W25Q16JV_H_ */
