/** @file serverApi.h
    @brief Header to the server.
*/

#ifndef _SERVERAPI_H_
#define _SERVERAPI_H_

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiClientSecureBearSSL.h>
#include <NTPClient.h>
#include <WiFiUdp.h>
#include <ArduinoJson.h>
#include "W25Q16JV.h"
#include "messages.h"
#include "CRC.h"

/* Configurações Firebase */
#define FIREBASE_HOST   "https://wirelessrht.firebaseio.com"
#define FIREBASE_AUTH   "iUwJfO2uiu820Q2uzXMbBGqFM9iTUKsYDpokPcbI"
#define FIREBASE_CLIENT "AarXB9uiCDfQX53NqsXJbLY2Umz2"  /* gianzanuz@gmail.com */

/* Pacote de mensagens */
#define MESSAGE_PACKAGE_COUNT   (1)
#define SERVER_TX_DELAY         (15000)
#define DEBUG_OUTPUT

class ServerApi {
    public:
        struct package_t {
            Messages::payload_t payload;
            /* ... */
            uint32_t            seqNumber;
            /* ... */
            uint32_t            reserved0;
            uint32_t            timestamp;
            /* ... */
            uint16_t            reserved2;
            uint16_t            crc16;
        };

        struct control_t {
            uint32_t    headCount;
            uint32_t    currentCount;
        };

        ServerApi() = default;
        bool begin();
        bool proccess_RX(Messages::payload_t* payload, uint32_t timestamp);
        bool proccess_TX();
        bool http_POST(package_t* messagePackageBuffer);
        
    private:
        W25Q16JV        w25q16jv;
        control_t       flashControl        = {0, 0};
        package_t       messagePackage[MESSAGE_PACKAGE_COUNT];
        uint8_t         messagePackageCount = 0;
        uint32_t        messageSeqNumber    = 0;
};

#endif  /* _SERVERAPI_H_ */
