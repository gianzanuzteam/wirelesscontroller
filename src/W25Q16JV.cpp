/** @file W25Q16JV.c 
 *  @brief Functions related with W25Q16JV NOR Serial Flash SPI by Winbond.
 */
 
#include "W25Q16JV.h"
#include <SPI.h>

/*************************************************************************************
* Private variables
*************************************************************************************/
SPISettings spiSettings(10000000, MSBFIRST, SPI_MODE0);

/*************************************************************************************
* Private macros
*************************************************************************************/
#define CS_FLASH            (5)    /* GPIO5 */
#define ATIVA_W25Q16JV()      digitalWrite(CS_FLASH, LOW)
#define DESATIVA_W25Q16JV()   digitalWrite(CS_FLASH, HIGH)

/*************************************************************************************
* Private type definitions
*************************************************************************************/
enum W25Q16JV_reg_pointer_t
{
    W25Q16JV_WRITE_ENABLE                       = 0x06,
    W25Q16JV_VOLATILE_SR_WRITE_ENABLE           = 0x50,
    W25Q16JV_WRITE_DISABLE                      = 0x04,
    /* ---- */
    W25Q16JV_RELEASE_POWER_DOWN                 = 0xAB,
    W25Q16JV_MANUFACTURER_ID                    = 0x90,
    W25Q16JV_JEDEC_ID                           = 0x9F,
    W25Q16JV_READ_UNIQUE_ID                     = 0x4B,
    /* ---- */
    W25Q16JV_READ_DATA                          = 0x03,
    W25Q16JV_FAST_READ                          = 0x0B,
    /* ---- */
    W25Q16JV_PAGE_PROGRAM                       = 0x02,
    /* ---- */
    W25Q16JV_SECTOR_ERASE_4KB                   = 0x20,
    W25Q16JV_BLOCK_ERASE_32KB                   = 0x52,
    W25Q16JV_BLOCK_ERASE_64KB                   = 0xD8,
    W25Q16JV_CHIP_ERASE                         = 0xC7,
    /* ---- */
    W25Q16JV_READ_STATUS_REG1                   = 0x05,
    W25Q16JV_WRITE_STATUS_REG1                  = 0x01,
    W25Q16JV_READ_STATUS_REG2                   = 0x35,
    W25Q16JV_WRITE_STATUS_REG2                  = 0x31,
    W25Q16JV_READ_STATUS_REG3                   = 0x15,
    W25Q16JV_WRITE_STATUS_REG3                  = 0x11,
    /* ---- */
    W25Q16JV_READ_SFDP_REG                      = 0x5A,
    W25Q16JV_ERASE_SEC_REG                      = 0x44,
    W25Q16JV_PROGRAM_SEC_REG                    = 0x42,
    W25Q16JV_READ_SEC_REG                       = 0x48,
    /* ---- */
    W25Q16JV_GLOBAL_BLOCK_LOCK                  = 0x7E,
    W25Q16JV_GLOBAL_BLOCK_UNLOCK                = 0x98,
    W25Q16JV_READ_BLOCK_LOCK                    = 0x3D,
    W25Q16JV_INDIVIDUAL_BLOCK_LOCK              = 0x36,
    W25Q16JV_INDIVIDUAL_BLOCK_UNLOCK            = 0x39,
    /* ---- */
    W25Q16JV_ERASE_PROGRAM_SUSPEND              = 0x75,
    W25Q16JV_ERASE_PROGRAM_RESUME               = 0x7A,
    W25Q16JV_POWER_DOWN                         = 0xB9,
    /* ---- */
    W25Q16JV_ENABLE_RESET                       = 0x66,
    W25Q16JV_RESET_DEVICE                       = 0x99,
};

enum W25Q16JV_reg_status1_t 
{
    W25Q16JV_STATUS1_NONE                       = 0x00,
    W25Q16JV_STATUS1_BUSY                       = 0x01,
    W25Q16JV_STATUS1_WEL                        = 0x02,
    W25Q16JV_STATUS1_BP0                        = 0x04,
    W25Q16JV_STATUS1_BP1                        = 0x08,
    W25Q16JV_STATUS1_BP2                        = 0x10,
    W25Q16JV_STATUS1_TB                         = 0x20,
    W25Q16JV_STATUS1_SEC                        = 0x40,
    W25Q16JV_STATUS1_SRP                        = 0x80,
};

enum W25Q16JV_reg_status2_t 
{
    W25Q16JV_STATUS2_NONE                       = 0x00,
    W25Q16JV_STATUS2_SRL                        = 0x01,
    W25Q16JV_STATUS2_QE                         = 0x02,
    W25Q16JV_STATUS2_RES                        = 0x04,
    W25Q16JV_STATUS2_LB1                        = 0x08,
    W25Q16JV_STATUS2_LB2                        = 0x10,
    W25Q16JV_STATUS2_LB3                        = 0x20,
    W25Q16JV_STATUS2_CMP                        = 0x40,
    W25Q16JV_STATUS2_SUS                        = 0x80,
};

enum W25Q16JV_reg_status3_t 
{
    W25Q16JV_STATUS3_NONE                       = 0x00,
    W25Q16JV_STATUS3_RES1                       = 0x01,
    W25Q16JV_STATUS3_RES2                       = 0x02,
    W25Q16JV_STATUS3_WPS                        = 0x04,
    W25Q16JV_STATUS3_RES3                       = 0x08,
    W25Q16JV_STATUS3_RES4                       = 0x10,
    W25Q16JV_STATUS3_DRV2                       = 0x20,
    W25Q16JV_STATUS3_DRV1                       = 0x40,
    W25Q16JV_STATUS3_RES5                       = 0x80,
};

/*******************************************************************************
   W25Q16JV_init
****************************************************************************//**
 * @brief Initializates the W25Q16JV NOR Serial Flash and SPI bus. 
 * @param void.
 * @return TRUE No errors occurred.
           FALSE Otherwise.
*******************************************************************************/
bool W25Q16JV::init(void)
{
    /* Verifica se já foi inicializado */
    static bool initialized = false;
    if(initialized)
        return true;
    initialized = true;

    /* Acorda do Sleep */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_RELEASE_POWER_DOWN);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();
    delay(50);      /* Delay necessário */
    isAwake = true;     /* Ativa flag de acordado */
    
    /* Obtém JEDEC ID */
    /* Verifica com valor esperado */
    uint8_t status[3] = {0};
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_JEDEC_ID);
      for(unsigned int i=0; i<sizeof(status); i++)
        status[i] = SPI.transfer(0xFF);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();

    /* Verificar sanidade com JEDEC ID */
    if(memcmp(status, static_cast<const void*>(JEDEC_ID), 3))
        return false;

//    /* Obtém UNIQUE ID */
//    uint8_t ID[8] = {0};
//    ATIVA_W25Q16JV();
//      SPI.transfer(W25Q16JV_READ_UNIQUE_ID);
//      SPI_Receive_byte();
//      SPI_Receive_byte();
//      SPI_Receive_byte();
//      SPI_Receive_byte();
//      SPI_Receive_array(ID, 8);
//    DESATIVA_W25Q16JV();

    /* Entra em power-down */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
       SPI.transfer(W25Q16JV_POWER_DOWN);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();
    isAwake = false;     /* Desativa flag de acordado */

    return true;
}

/*******************************************************************************
   read_data
****************************************************************************//**
 * @brief Reads data from the W25Q16JV NOR Serial Flash.
 * @param  
 * @return TRUE No errors occurred.
           FALSE Otherwise.
*******************************************************************************/
bool W25Q16JV::read_data(uint8_t* data, uint32_t size, uint32_t address)
{
    /* Verifica se o endereço é maior que a capacidade */
    if(address >= W25Q16JV_MAX_ADDRESS_TOTAL)
        return false;
    
    /* Caso esteja em power-down, acorda */
    if(!isAwake)
        wakeup();
        
    /* Leitura da página na FLASH */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_READ_DATA);
      SPI.transfer((address >> 16) & 0xFF);
      SPI.transfer((address >>  8) & 0xFF);
      SPI.transfer((address >>  0) & 0xFF);
      for(unsigned int i=0; i<size; i++)
        data[i] = SPI.transfer(0xFF);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();

    return true;
}

/*******************************************************************************
   write_data
****************************************************************************//**
 * @brief Writes data from the buffer to the W25Q16JV NOR Serial Flash.
 * @param  
 * @return TRUE No errors occurred.
           FALSE Otherwise.
*******************************************************************************/
bool W25Q16JV::write_data(uint8_t* data, uint32_t size, uint32_t address)
{
    /* Datasheet: 0.4 ~ 3ms (MAX) */
    /* Considerando ~5 ciclo clock/instrução, Clock 48MHz */
    /* (48e3*3)/5 = 28800 */
    uint32_t timeout = 28800;

    /* Verifica se o endereço é maior que a capacidade */
    if(address >= W25Q16JV_MAX_ADDRESS_TOTAL)
        return false;
    
    /* Caso esteja em power-down, acorda */
    if(!isAwake)
        wakeup();
    
    /* Verifica se é início do setor */
    /* Caso afirmativo, apaga setor */
    if(!(address % W25Q16JV_SECTOR_BYTE_SIZE))
        erase_sector(address);

    /* Habilita a escrita */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_WRITE_ENABLE);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();
    
    /* Escrita do buffer para a FLASH */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_PAGE_PROGRAM);
      SPI.transfer((address >> 16) & 0xFF);
      SPI.transfer((address >>  8) & 0xFF);
      SPI.transfer((address >>  0) & 0xFF);
      SPI.transfer(data, size);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();

    /* Aguarda finalizar comando */
    /* Utiliza um timeout de verificação */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_READ_STATUS_REG1);
      while(timeout && (W25Q16JV_STATUS1_BUSY & SPI.transfer(0xFF)))
          timeout--;
    DESATIVA_W25Q16JV();
    SPI.endTransaction();
        
    /* A variavel bool é do tipo uint8_t, não misturar com uint32_t */
    /* e.g. (uint8_t) 256*n = 0 */
    return (timeout > 0);
}

/*******************************************************************************
   erase_sector
****************************************************************************//**
 * @brief Erases a sector (4KB) from FLASH of W25Q16JV NOR Serial Flash.
 * @param  
 * @return TRUE No errors occurred.
           FALSE Otherwise.
*******************************************************************************/
bool W25Q16JV::erase_sector(uint32_t address)
{
    /* Datasheet: 45 ~ 400ms (MAX) */
    /* Considerando ~5 ciclo clock/instrução, Clock 48MHz */
    /* (48e3*400)/5 = 3840000 */
    uint32_t timeout = 3840000;
    
    /* Verifica se o endereço é maior que a capacidade */
    if(address >= W25Q16JV_MAX_ADDRESS_TOTAL)
        return false;

    /* Caso esteja em power-down, acorda */
    if(!isAwake)
        wakeup();
    
    /* Habilita a escrita */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_WRITE_ENABLE);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();
    
    /* Apaga apenas um setor de 4KB da FLASH */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_SECTOR_ERASE_4KB);
      SPI.transfer((address >> 16) & 0xFF);
      SPI.transfer((address >>  8) & 0xFF);
      SPI.transfer((address >>  0) & 0xFF);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();

    /* Aguarda finalizar comando */
    /* Utiliza um timeout de verificação */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_READ_STATUS_REG1);
      while(timeout && (W25Q16JV_STATUS1_BUSY & SPI.transfer(0xFF)))
          timeout--;
    DESATIVA_W25Q16JV();
    SPI.endTransaction();

    /* A variavel bool é do tipo uint8_t, não misturar com uint32_t */
    /* e.g. (uint8_t) 256*n = 0 */
    return (timeout > 0);
}

/*******************************************************************************
   erase_chip
****************************************************************************//**
 * @brief Erases the entire FLASH from W25Q16JV NOR Serial Flash.
 * @param  
 * @return TRUE No errors occurred.
           FALSE Otherwise.
*******************************************************************************/
bool W25Q16JV::erase_chip(uint32_t address)
{
    /* Datasheet: 5 ~ 25s (MAX) */
    /* Considerando ~5 ciclo clock/instrução, Clock 48MHz */
    /* (48e3*25000)/5 = 240000000 */
    uint32_t timeout = 240000000;

    /* Verifica se o endereço é maior que a capacidade */
    if(address >= W25Q16JV_MAX_ADDRESS_TOTAL)
        return false;

    /* Caso esteja em power-down, acorda */
    if(!isAwake)
        wakeup();
    
    /* Habilita a escrita */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_WRITE_ENABLE);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();
    
    /* Apaga todo o conteúdo da FLASH */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_CHIP_ERASE);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();

    /* Aguarda finalizar comando */
    /* Utiliza um timeout de verificação */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_READ_STATUS_REG1);
      while(timeout && (W25Q16JV_STATUS1_BUSY & SPI.transfer(0xFF)))
          timeout--;
    DESATIVA_W25Q16JV();
    SPI.endTransaction();

    /* A variavel bool é do tipo uint8_t, não misturar com uint32_t */
    /* e.g. (uint8_t) 256*n = 0 */
    return (timeout > 0);
}

/*******************************************************************************
   wakeup
****************************************************************************//**
 * @brief Wakes up the W25Q16JV NOR Serial Flash.
 * @param void.
 * @return void.
*******************************************************************************/
void W25Q16JV::wakeup(void)
{
    /* Caso já esteja acordado, retorna */
    if(isAwake)
        return;
    isAwake = true;
    
    /* Acorda do Sleep */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_RELEASE_POWER_DOWN);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();
    delay(10); /* Delay necessário */
}

/*******************************************************************************
   sleep
****************************************************************************//**
 * @brief Power down the W25Q16JV NOR Serial Flash.
 * @param void.
 * @return void.
*******************************************************************************/
void W25Q16JV::sleep(void)
{
    /* Caso já esteja em power-down, retorna */
    if(!isAwake)
        return;
    isAwake = false;
    
    /* Entra em power-down */
    SPI.beginTransaction(spiSettings);
    ATIVA_W25Q16JV();
      SPI.transfer(W25Q16JV_POWER_DOWN);
    DESATIVA_W25Q16JV();
    SPI.endTransaction();
    delay(10); /* Delay necessário */
}
