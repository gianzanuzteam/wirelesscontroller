#include "serverApi.h"

/*******************************************************************************
   begin
****************************************************************************/
/**
 * @brief 
 * @param
 * @return
*******************************************************************************/
bool ServerApi::begin(void)
{
    /* Inicializa memória flash */
    w25q16jv.init();

    return true;
}

/*******************************************************************************
   proccess_RX
****************************************************************************/
/**
 * @brief 
 * @param
 * @return
*******************************************************************************/
bool ServerApi::proccess_RX(Messages::payload_t *payload, uint32_t timestamp)
{
    /* Verifica CRC-8 */
    if (payload->crc8 != CRC_8((uint8_t *)payload, sizeof(Messages::payload_t) - 1, CRC_8_MAXIM_POLY))
        return false;

#ifdef DEBUG_OUTPUT
    Serial.println("DEBUG: CRC8 OK");
#endif

    /* Adiciona payload ao pacote */
    messagePackage[messagePackageCount].payload = *payload;
    messagePackage[messagePackageCount].seqNumber = messageSeqNumber++;
    messagePackage[messagePackageCount].reserved0 = 0;
    messagePackage[messagePackageCount].timestamp = timestamp;
    messagePackage[messagePackageCount].reserved2 = 0;
    messagePackage[messagePackageCount].crc16 = CRC_16((uint8_t *)&messagePackage[messagePackageCount], sizeof(package_t) - 2, CRC_16_IBM_POLY);

    /* Verifica se atingiu a quantidade necessária de mensagens para salvar o pacote */
    messagePackageCount++;
    if (messagePackageCount >= MESSAGE_PACKAGE_COUNT)
    {
        /* Salva na memória FLASH */
        w25q16jv.write_data((uint8_t *)messagePackage, sizeof(messagePackage), (flashControl.headCount % W25Q16JV_MAX_PAGE_COUNT) * W25Q16JV_PAGE_BYTE_SIZE);
        flashControl.headCount++;

#ifdef DEBUG_OUTPUT
        Serial.println("DEBUG: SAVE FLASH");
#endif

        /* Restaura buffer de armazenamento de mensagens */
        memset(messagePackage, 0, sizeof(messagePackage));
        messagePackageCount = 0;
    }

    return true;
}

/*******************************************************************************
   proccess_TX
****************************************************************************/
/**
 * @brief 
 * @param
 * @return
*******************************************************************************/
bool ServerApi::proccess_TX(void)
{
    /* Verifica se já passou período mínimo */
    static uint32_t lastTx = 0;
    if (millis() < (lastTx + SERVER_TX_DELAY))
        return false;
    lastTx = millis();

    /* Verifica se está conectado à rede WiFi */
    if (!WiFi.isConnected())
        return false;

    /* Enquanto o ponteiro 'head' for maior que o atual */
    while (flashControl.headCount > flashControl.currentCount)
    {
        /* Obtém da memória FLASH */
        package_t messagePackageBuffer[MESSAGE_PACKAGE_COUNT];
        if (!w25q16jv.read_data((uint8_t *)messagePackageBuffer, sizeof(messagePackageBuffer), (flashControl.currentCount % W25Q16JV_MAX_PAGE_COUNT) * W25Q16JV_PAGE_BYTE_SIZE))
            return false;

#ifdef DEBUG_OUTPUT
        Serial.println("DEBUG: LOAD FLASH");
#endif

        /* Envia cada mensagem do pacote */
        for (int i = 0; i < MESSAGE_PACKAGE_COUNT; i++)
        {
            /* Verifica CRC-16 */
            if (messagePackageBuffer[i].crc16 != CRC_16((uint8_t *)&messagePackageBuffer[i], sizeof(package_t) - 2, CRC_16_IBM_POLY))
                continue;

#ifdef DEBUG_OUTPUT
            Serial.println("DEBUG: CRC16 OK");
#endif

            /* Envia para servidor */
            if (!http_POST(&messagePackageBuffer[i]))
                return false;
        }

        /* Incrementa quantidade de mensagens enviadas */
        flashControl.currentCount++;
    }

    return true;
}

/*******************************************************************************
   http_POST
****************************************************************************/
/**
 * @brief
 * @param
 * @return
*******************************************************************************/
bool ServerApi::http_POST(package_t *messagePackageBuffer)
{
    /* Formata sourceId */
    char sourceId[15];
    sprintf(sourceId, "%02X:%02X:%02X:%02X:%02X", messagePackageBuffer->payload.sourceId[0],
            messagePackageBuffer->payload.sourceId[1],
            messagePackageBuffer->payload.sourceId[2],
            messagePackageBuffer->payload.sourceId[3],
            messagePackageBuffer->payload.sourceId[4]);

    /* Certificado ssl */
    std::unique_ptr<BearSSL::WiFiClientSecure> client(new BearSSL::WiFiClientSecure);
    client->setInsecure();

    /* Conexão http persistente */
    HTTPClient https;
    https.setReuse(true);

    /* Para cada medida */
    for (int i = 0; i < MEASURE_SIZE; i++)
    {
        /* Verifica medida */
        if (messagePackageBuffer->payload.measures[i].type == Measures::MEASURE_NONE)
            continue;

        /* Cria cópia dos valores */
        /* Necessário devido ao alinhamento de bytes */
        float value = messagePackageBuffer->payload.measures[i].value;
        uint8_t type = messagePackageBuffer->payload.measures[i].type;

        /* Cria json a partir do pacote */
        DynamicJsonDocument doc(128);
        doc["value"] = value;
        doc["type"] = type;
        doc["seqNumber"] = messagePackageBuffer->seqNumber;
        doc["timestamp"] = messagePackageBuffer->timestamp;
        doc["device"] = sourceId;

        /* Conecta ao cliente */
        String url = FIREBASE_HOST "/users/" FIREBASE_CLIENT "/measures/" + String(Measures::typeString((Measures::types_t) type)) + ".json?auth=" FIREBASE_AUTH;
        if(!https.begin(*client, url))
        {
#ifdef DEBUG_OUTPUT
            Serial.println("DEBUG: CONNECT FAIL");
#endif
            return false;
        }

        String body;
        serializeJson(doc, body);
        doc.clear();
        if (https.POST(body) != HTTP_CODE_OK)
        {
#ifdef DEBUG_OUTPUT
            Serial.println("DEBUG: SERVER NO ACK");
#endif
            return false;
        }

#ifdef DEBUG_OUTPUT
        Serial.println("DEBUG: SERVER ACK RECEIVED");
#endif
    }

    /* Finaliza conexão */
    https.end();

    return true;
}
